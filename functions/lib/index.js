"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const admin = require("firebase-admin");
// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
admin.initializeApp(functions.config().firebase);
exports.atualizarMensagens = functions.firestore.document('mensagens/{mensagemId}').onCreate((event) => __awaiter(this, void 0, void 0, function* () {
    let data = event.data();
    let duvidaId = data.duvida;
    let doc = yield admin.firestore().collection("duvidas").doc(duvidaId).get();
    if (doc.exists) {
        let mensagensCount = doc.data().mensagensCount || 0;
        mensagensCount++;
        yield admin.firestore().collection("duvidas").doc(duvidaId).update({
            "mensagensCount": mensagensCount
        });
        return true;
    }
    else {
        return false;
    }
}));
//# sourceMappingURL=index.js.map