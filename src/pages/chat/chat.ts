import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController, ActionSheetController, AlertController, ModalController, LoadingController } from 'ionic-angular';
import firebase, { database } from 'firebase';
import moment from 'moment'


@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {
  
  duvida: any = {};
  mensagens: any[] = [];
  mensagemChat: string = "";
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private viewCtrl: ViewController, 
    private toastCtrl: ToastController) {
      
      console.log(this.duvida.id)
      this.duvida = this.navParams.get("duvida");
      console.log(this.duvida)
      this.getMensagem();
    }

    enviarMensagem(duvida){
      console.log(duvida)
      firebase.firestore().collection("mensagens").add({
        text: this.mensagemChat, 
        duvida : duvida.id,
        owner: firebase.auth().currentUser.uid,
        owner_name: firebase.auth().currentUser.displayName,
        created: firebase.firestore.FieldValue.serverTimestamp()
        
      }).then((doc) =>{
        this.toastCtrl.create({
          message: "Mensagem enviada com sucesso",
          duration: 3000
        }).present();
     
      }).catch((erro) =>{
        this.toastCtrl.create({
          message: erro.message,
          duration: 3000
        }).present();
      })
      this.getMensagem();
      this.mensagemChat = "";
    }
        
    getTempo(time){
      let tempo = moment(time).diff(moment());  
      return moment.duration(tempo).humanize();
    }

    getMensagem(){
      firebase.firestore().collection("mensagens")
      .where("duvida", "==", this.duvida.id)
      .orderBy("created", "asc")
      .get()
      .then((data) =>{
        this.mensagens = data.docs;
      }).catch((erro) =>{
        console.log(erro)
      })
    }

    fechar(){
      this.viewCtrl.dismiss();
    }


    
    
  }
  