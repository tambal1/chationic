import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ToastController, LoadingController } from 'ionic-angular';
import firebase from 'firebase';
import { LoginPage } from '../login/login';
import { UsuarioProvider } from '../../providers/usuario/usuario';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-cadastro',
  templateUrl: 'cadastro.html',
})
export class CadastroPage {
  
  
  usuario: string = 'É advogado?';
  hideMe: boolean = false;
  
  novoAdvogado = {
    email: '',
    senha: '',
    confirmarSenha: '',
    nome: '',
    oab: '',
    celular:''
  }
  
  novoUsuario = {
    email: '',
    senha: '',
    confirmarSenha: '',
    nome: ''
  }
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private loadingCtrl: LoadingController,
    private usuarioService: UsuarioProvider) {

    }
    
    ionViewDidLoad() {
      console.log('ionViewDidLoad CadastroPage');
    }
    
    criarUsuario(){
      if(this.novoUsuario.senha != this.novoUsuario.confirmarSenha){
        console.log("As senhas são diferentes");
        
      }else{
        let loader = this.loadingCtrl.create({
          content: 'Por favor, espere um momento'
        });
        loader.present();
        this.usuarioService.addUsuario(this.novoUsuario).then((res:any) =>{
          loader.dismiss();
          if (res.success)
          this.navCtrl.push(HomePage);
          else
          alert('Error' + res);
        })
      }
    }
    
    criarAdvogado(){
      if(this.novoAdvogado.senha != this.novoAdvogado.confirmarSenha){
        console.log("As senhas não correspondem");
        
      }else{
        let loader = this.loadingCtrl.create({
          content: 'Por favor, espere um momento'
        });
        loader.present();
        this.usuarioService.addAdvogado(this.novoAdvogado).then((res:any) =>{
          loader.dismiss();
          if (res.success)
          this.navCtrl.push(HomePage);
          else
          alert('Error' + res);
        })
      }
    }
    
    
    hide() {
      if(this.hideMe == true){
        this.hideMe = false; 
        this.usuario = "É advogado?"
      }
      else{ 
        this.hideMe = true 

        this.usuario = "Não é advogado?"
      }
    }
    
  }
  