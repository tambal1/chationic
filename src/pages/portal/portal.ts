import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, ActionSheetController, AlertController, ModalController } from 'ionic-angular';
import firebase from 'firebase';
import moment from 'moment';
import { LoginPage } from '../login/login';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { HttpClient } from '@angular/common/http';
import { HomePage } from '../home/home';
import { AdvogadoFeedPage } from '../advogado-feed/advogado-feed';


@IonicPage()
@Component({
  selector: 'page-portal',
  templateUrl: 'portal.html',
})
export class PortalPage {
  
  text: string = "";
  duvidas: any [] = [];       
  pageSize: number = 5;
  cursor: any;
  infiniteEvent: any;        
  image: string;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private camera: Camera,
    private http: HttpClient,
    private actionSheetCtrl: ActionSheetController,
    private alertCtrl: AlertController,
    private modalCtrl: ModalController) {
      this.getDuvidas();
    }

    home(){
      this.navCtrl.push(HomePage);
    }
    
    getDuvidas(){
      this.duvidas = [];
      let loading =  this.loadingCtrl.create({
        content:"Carregando as suas perguntas"
      })
      loading.present()
      let query = firebase.firestore().collection("duvidas").orderBy("created", "desc")
      .limit(this.pageSize);
      query.get()
      .then((docs) => {
        docs.forEach((doc) => {
          this.duvidas.push(doc);
        })
        loading.dismiss();
        this.cursor = this.duvidas[this.duvidas.length -1];
      }).catch((erro) => {
        console.log(erro)
      }) 
    }
    
    carregarDuvidas(event){
      firebase.firestore().collection("duvidas").orderBy("created", "desc").startAfter(this.cursor).limit(this.pageSize).get()
      .then((docs) => {
        
        docs.forEach((doc) => {
          this.duvidas.push(doc);
        })
        
        if(docs.size < this.pageSize){
          event.enable(false);
          this.infiniteEvent = event;
          
        }else{
          event.complete();
          this.cursor = this.duvidas[this.duvidas.length -1];
        }
        
      }).catch((erro) => {
        console.log(erro)
      })
    }
    
    post(){
      
      firebase.firestore().collection("duvidas").add({
        text: this.text,
        created: firebase.firestore.FieldValue.serverTimestamp(),
        owner: firebase.auth().currentUser.uid,
        owner_name: firebase.auth().currentUser.displayName
      }).then(async (doc) =>{      
        console.log(doc)
        if(this.image){
          await this.upload(doc.id)
        }
        this.text = "";
        this.image = undefined;
        let toast = this.toastCtrl.create({
          message: "Sua mensagem foi publicada com sucesso",
          duration: 3000
        }).present()
        this.getDuvidas()
      }).catch((erro) =>{
        console.log(erro);
      })
    }
    
    getTempo(time){
      let tempo = moment(time).diff(moment());  
      return moment.duration(tempo).humanize();
    }
    
    atualizar(event){
      this.getDuvidas();
      event.complete();
      if(this.infiniteEvent){
        this.infiniteEvent.enable(true)
      }
    }

    sair(){
      firebase.auth().signOut().then(() =>{
        let toast = this.toastCtrl.create({
          message:"Você foi deslogado com sucesso",
          duration: 3000
        }).present()
        
        this.navCtrl.setRoot(LoginPage);
      });
    }

    addFoto(){
      this.carregarCamera();
    }
    
    carregarCamera(){
      let options: CameraOptions = {
        quality:100,
        sourceType: this.camera.PictureSourceType.CAMERA,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.PNG,
        mediaType: this.camera.MediaType.PICTURE,
        correctOrientation: true,
        targetHeight: 512,
        targetWidth: 512,
        allowEdit: true
      }

      this.camera.getPicture(options).then((base64Image) => {
        console.log(base64Image);
        this.image = "data:image/png;base64," + base64Image;
      }).catch((erro) => {
        console.log(erro)
      })
    }
    
    upload(name : string){
      return new Promise((resolve, reject) => {
        let loading = this.loadingCtrl.create({
          content: "Carregando imagens"
        })
        let ref = firebase.storage().ref("imagemDuvida/" + name);
        let uploadTask = ref.putString(this.image.split(',')[1],"base64");
        uploadTask.on("state_changed", (taskSnapshot: any) =>{
          console.log(taskSnapshot)
          let percentage = taskSnapshot.bytesTrasferred / taskSnapshot.totalBytes * 100;
          loading.setContent("Carregado" + percentage + "%")
        }, (erro) => {
          console.log(erro)
        }, ()=>{
          uploadTask.snapshot.ref.getDownloadURL().then((url) => {
            firebase.firestore().collection("duvidas").doc(name).update({
              image: url
            }).then(() => {
              loading.dismiss()
              resolve()
            }).catch((erro) => {
              loading.dismiss()
              reject()
            })
          }).catch((erro) => {
            loading.dismiss()
            reject()
          })
        })
      })
    }
    
    chatComment(duvida){
      this.modalCtrl.create('ChatPage', {"duvida": duvida}).present();
    }
  }
  