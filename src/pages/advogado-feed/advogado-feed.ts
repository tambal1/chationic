import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import firebase from 'firebase';
import { HttpClient } from '@angular/common/http';
import moment from 'moment';


@IonicPage()
@Component({
  selector: 'page-advogado-feed',
  templateUrl: 'advogado-feed.html',
})
export class AdvogadoFeedPage {

  text: string = "";
  duvidas: any [] = [];       
  pageSize: number = 5;
  cursor: any;
  infiniteEvent: any;        

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private loadingCtrl: LoadingController,
    private http: HttpClient) {
      this.getDuvidas();
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdvogadoFeedPage');
  }


  getDuvidas(){
    this.duvidas = [];
    let loading =  this.loadingCtrl.create({
      content:"Carregando as suas perguntas"
    })
    loading.present()
    let query = firebase.firestore().collection("duvidas").orderBy("created", "desc")
    .limit(this.pageSize);
    query.get()
    .then((docs) => {
      docs.forEach((doc) => {
        this.duvidas.push(doc);
      })
      loading.dismiss();
      this.cursor = this.duvidas[this.duvidas.length -1];
    }).catch((erro) => {
      console.log(erro)
    }) 
  }
  
  carregarDuvidas(event){
    firebase.firestore().collection("duvidas").orderBy("created", "desc").startAfter(this.cursor).limit(this.pageSize).get()
    .then((docs) => {
      
      docs.forEach((doc) => {
        this.duvidas.push(doc);
      })
      
      if(docs.size < this.pageSize){
        event.enable(false);
        this.infiniteEvent = event;
        
      }else{
        event.complete();
        this.cursor = this.duvidas[this.duvidas.length -1];
      }
      
    }).catch((erro) => {
      console.log(erro)
    })
  }

  getTempo(time){
    let tempo = moment(time).diff(moment());  
    return moment.duration(tempo).humanize();
  }

}
