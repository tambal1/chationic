import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { CadastroPage } from '../cadastro/cadastro';
import firebase from 'firebase';
import { PortalPage } from '../portal/portal';
import { HomePage } from '../home/home';
import { AdvogadoFeedPage } from '../advogado-feed/advogado-feed';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  email: string = "";
  senha: string = ""
  userId: string = "";


  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public toast: ToastController) {
      
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  login(){
    firebase.auth().signInWithEmailAndPassword(this.email, this.senha)
    .then((usuario)=>{
      console.log(usuario);
      this.toast.create({
        message:"Bem vindo " + usuario.user.displayName,
        duration: 3000
      }).present();      




      this.userId = firebase.auth().currentUser.uid;
      console.log(this.userId);
      var refe = firebase.firestore().collection('advogados').doc(this.userId);
      var getPage = refe.get().then(doc => {
        if (!doc.exists) {
          console.log("ADVOGADO NÃO ENCONTRADO")
          this.navCtrl.push(PortalPage)
        } else {
          console.log("ADVOGADO ENCONTRADO COM O ID" + this.userId);
          this.navCtrl.push(AdvogadoFeedPage)
        }
      }).catch(err => {
        console.log(err)
      });















    })
    .catch((erro) =>{
      this.toast.create({
        message: erro.message,
        duration: 3000
      }).present();
    })
  }


  cadastro = () =>{
    this.navCtrl.push(CadastroPage);
  }

  teste = () => {
    this.navCtrl.push(HomePage);
  }

}
