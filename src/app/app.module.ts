import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MyApp } from './app.component';

// IMPORTAÇÕES DAS PÁGINAS
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { CadastroPage } from '../pages/cadastro/cadastro';
import { PortalPage } from '../pages/portal/portal';
// import { ChatPage } from '../pages/chat/chat';

// IMPORTAÇÕES DO FIREBASE
import firebase from 'firebase';
import { HttpClientModule } from '@angular/common/http'
// IMPORTAÇÕES NATIVAS
import { Camera } from '@ionic-native/camera';
import { UsuarioProvider } from '../providers/usuario/usuario';
import { AdvogadoFeedPage } from '../pages/advogado-feed/advogado-feed';


var config = {
  apiKey: "AIzaSyACkuMYVbdYFa5C0MAyWsOCiiBFA2GiUcU",
  authDomain: "chatyoutb.firebaseapp.com",
  databaseURL: "https://chatyoutb.firebaseio.com",
  projectId: "chatyoutb",
  storageBucket: "chatyoutb.appspot.com",
  messagingSenderId: "561760823491"
};

firebase.initializeApp(config);
firebase.firestore().settings({
  timestampsInSnapshots: true
})

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    CadastroPage,
    PortalPage,
    AdvogadoFeedPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    CadastroPage,
    PortalPage,
    AdvogadoFeedPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UsuarioProvider
  ]
})
export class AppModule {}
